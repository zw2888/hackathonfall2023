# Datasheets for Datasets: MisInfoText_BuzzFeedWebis

## Motivation
1. For what purpose was the dataset created? Was there a specific task in mind? Was there a specific gap that needed to be filled? Please provide a description.

**The dataset was created to enhance research on fake news by integrating two popular datasets for fake news detection: MisInfoText (2016 US Election) and BuzzFeed-Webis. Intended applications include the studies of fake news detection, fake news evolution, and fake news mitigation.**

2. Who created the dataset (e.g., which team, research group) and on behalf of which entity (e.g., company, institution, organization)? 
**MisInfoText was created by Fatemeh Torabi Asr and Maite Taboada. BuzzFeed-Webis was created by Martin Potthast, Johannes Kiesel, Kevin Reinartz, Janek Bevendorff, and Benno Stein at Bauhaus-Universität Weimar. The integrated dataset was created by PressDB.**

4. Who funded the creation of the dataset? If there is an associated grant, please provide the name of the grantor and the grant name and number. 
**For MisInfoText, funding was provided by the Natural Sciences and Engineering Research Council of Canada and NVIDIA Corporation.**

5. Any other comments? 
**None.**

## Composition
1. What do the instances that comprise the dataset represent (e.g., documents, photos, people, countries)? Are there multiple types of instances (e.g., movies, users, and ratings; people and interactions between them; nodes and edges)? Please provide a description.
**The instances are news articles curated by journalists from BuzzFeed News. Each article is labeled as "mostly true" for articles considered to be mostly true, "mixture of true and false" for articles considered to have both true and false statements, "mostly false" for articles considered to be mostly false, and "no factual content" for articles that do not have central claims (ex. analysis articles), or for opinion pieces.**

2. How many instances are there in total (of each type, if appropriate)?
**There are 167 articles designated as "mixture of true and false", 63 designated as "mostly false", 1085 designated as "mostly true", and 56 designated as "no factual content".**

4. Does the dataset contain all possible instances or is it a sample (not necessarily random) of instances from a larger set? If the dataset is a sample, then what is the larger set? Is the sample representative of the larger set (e.g., geographic coverage)? If so, please describe how this representativeness was validated/verified. If it is not representative of the larger set, please describe why not (e.g., to cover a more diverse range of instances, because instances were withheld or unavailable).
**The dataset is a non-random sample of articles. The instances curated by BuzzFeed are a sub-sample of a significantly larger sample from https://github.com/BuzzFeedNews/2016-10-facebook-fact-check. To the knowledge of the authors of the datasheet, there were no tests to validate representativeness.**

5. What data does each instance consist of? "Raw" data (e.g., unprocessed text or images) or features? In either case, please provide a description.
**Each instance includes the article body, article metadata (title, author(s)), article URL, source metadata (source name, political slant), and a label for the article veracity.**

6. Is there a label or target associated with each instance? If so, please provide a description.
**The label relates to the veracity of the given article, per above.**

7. Is any information missing from individual instances? If so, please provide a description, explaining why this information is missing (e.g., because it was unavailable). This does not include intentionally removed information, but might include, e.g., redacted text.
**Some instances are missing corresponding article (meta)data due to extraction issues or lack of availability. There are also 8 articles that are in MisInfoText that are not in BuzzFeed-Webis, so the only information included is the article URL, article body, and article veracity label.**

8. Are relationships between individual instances made explicit (e.g., users’ movie ratings, social network links)? If so, please describe how these relationships are made explicit.
**No.**

9. Are there recommended data splits (e.g., training, development/validation, testing)? If so, please provide a description of these splits, explaining the rationale behind them.
**No.**

10. Are there any errors, sources of noise, or redundancies in the dataset? If so, please provide a description.
- **Nine articles have different veracity labels within MisInfoText.**
- **10 articles have different veracity ratings between MisInfoText and BuzzFeed-Webis**.
- **17 articles from MisInfoText had incorrect article bodies.**

10. Is the dataset self-contained, or does it link to or otherwise rely on external resources (e.g., websites, tweets, other datasets)? If it links to or relies on external resources, a) are there guarantees that they will exist, and remain constant, over time; b) are there official archival versions of the complete dataset (i.e., including the external resources as they existed at the time the dataset was created); c) are there any restrictions (e.g., licenses, fees) associated with any of the external resources that might apply to a dataset consumer? Please provide descriptions of all external resources and any restrictions associated with them, as well as links or other access points, as appropriate.
**The dataset is self-contained. However, not all of the article URLs provided are saved on web archiving sites, so it is not guaranteed that the articles will exist and remain constant over time.**

11. Does the dataset contain data that might be considered confidential (e.g., data that is protected by legal privilege or by doctor–patient confidentiality, data that includes the content of individuals’ non-public communications)? If so, please provide a description. **Unknown to the authors of the datasheet.**

12. Does the dataset contain data that, if viewed directly, might be offensive, insulting, threatening, or might otherwise cause anxiety? If so, please describe why.
**Some articles may contain offensive language.**

13. Does the dataset identify any subpopulations (e.g., by age, gender)? If so, please describe how these subpopulations are identified and provide a description of their respective distributions within the dataset. 
**No.**

14. Is it possible to identify individuals (i.e., one or more natural persons), either directly or indirectly (i.e., in combination with other data) from the dataset? If so, please describe how.
**Article author names, as well as any persons described in the news articles, are included in the dataset. This information is already public as it has been posted on news websites.**

15. Does the dataset contain data that might be considered sensitive in any way (e.g., data that reveals race or ethnic origins, sexual orientations, religious beliefs, political opinions or union memberships, or locations; financial or health data; biometric or genetic data; forms of government identification, such as social security numbers; criminal history)? If so, please provide a description. 
**Unknown to the authors of the datasheet.**

16. Any other comments? 
**None.**

## Collection Process
1. How was the data associated with each instance acquired? Was the data directly observable (e.g., raw text, movie ratings), reported by subjects (e.g., survey responses), or indirectly inferred/derived from other data (e.g., part-of-speech tags, model-based guesses for age or language)? If the data was reported by subjects or indirectly inferred/derived from other data, was the data validated/verified? If so, please describe how.
**For the articles curated by BuzzFeed News, they were collected over a period of seven weekdays in September 2016 from nine verified news sources (three left-leaning, three right-leaning, and three mainstream) on Facebook. The MisInfoText and BuzzFeed-Webis authors in turn collected more data about these articles from BuzzFeed (via https://github.com/BuzzFeedNews/2016-10-facebook-fact-check).**

2. What mechanisms or procedures were used to collect the data (e.g., hardware apparatuses or sensors, manual human curation, software programs, software APIs)? How were these mechanisms or procedures validated?
**The article data was collected by the MisInfoText and BuzzFeed-Webis authors via web crawling the news article URLs. How these processes were validated is unknown to the authors of the datasheet.**

3. If the dataset is a sample from a larger set, what was the sampling strategy (e.g., deterministic, probabilistic with specific sampling probabilities)?
**The dataset was not sampled from a larger set.**

4. Who was involved in the data collection process (e.g., students, crowdworkers, contractors) and how were they compensated (e.g., how much were crowdworkers paid)? 
**Unknown to the authors of the datasheet.**

5. Over what timeframe was the data collected? Does this timeframe match the creation timeframe of the data associated with the instances (e.g., recent crawl of old news articles)? If not, please describe the timeframe in which the data associated with the instances was created. 
**The articles curated by BuzzFeed News were collected in September 2016. The exact timeframe that the MisInfoText and BuzzFeed-Webis authors collected the data is unknown to the authors of the datasheet.**

6. Were any ethical review processes conducted (e.g., by an institutional review board)? If so, please provide a description of these review processes, including the outcomes, as well as a link or other access point to any supporting documentation. 
**Unknown to the authors of the datasheet.**

7. Did you collect the data from the individuals in question directly, or obtain it via third parties or other sources (e.g., websites)? 
**Per above, the data was collected from BuzzFeed News.**

8. Were the individuals in question notified about the data collection? If so, please describe (or show with screenshots or other information) how notice was provided, and provide a link or other access point to, or otherwise reproduce, the exact language of the notification itself.
**No. The data was collected from public web sources. Whether the news publishers, journalists or individuals were notified that the data would be collected is unknown to the authors of the datasheet.**

9. Did the individuals in question consent to the collection and use of their data? If so, please describe (or show with screenshots or other information) how consent was requested and provided, and provide a link or other access point to, or otherwise reproduce, the exact language to which the individuals consented. 
**No, per above.**

10. If consent was obtained, were the consenting individuals provided with a mechanism to revoke their consent in the future or for certain uses? If so, please provide a description, as well as a link or other access point to the mechanism (if appropriate). 
**N/A.**

11. Has an analysis of the potential impact of the dataset and its use on data subjects (e.g., a data protection impact analysis) been conducted? If so, please provide a description of this analysis, including the outcomes, as well as a link or other access point to any supporting documentation. 
**N/A.**

12. Any other comments? 
**None.**

## Preprocessing/cleaning/labeling
1. Was any preprocessing/cleaning/labeling of the data done (e.g., discretization or bucketing, tokenization, part-of-speech tagging, SIFT feature extraction, removal of instances, processing of missing values)? If so, please provide a description. If not, you may skip the remaining questions in this section. 
**Data cleaning process of original authors is unknown to the authors of the datasheet. Article URLs from both datasets were standardized before the datasets were merged. For the 10 articles with different veracity labels, they were manually reviewed and updated according to best judgment. For the 17 MisInfoText articles with incorrect article bodies, the corresponding article bodies from BuzzFeed-Webis were used.**

2. Was the “raw” data saved in addition to the preprocessed/cleaned/labeled data (e.g., to support unanticipated future uses)? If so, please provide a link or other access point to the “raw” data. 
**N/A.**

3. Is the software that was used to preprocess/clean/label the data available? If so, please provide a link or other access point. 
**N/A.**

4. Any other comments? 
**None.**

## Uses
1. Has the dataset been used for any tasks already? If so, please provide a description. 
**As of 2023-09-08, at least 83 papers have cited MisInfoText (https://www.semanticscholar.org/paper/Big-Data-and-quality-data-for-fake-news-and-Asr-Taboada/b2239452680e97c503a90f62ccdc8137a893b1e9#citing-papers) and 498 papers have cited BuzzFeed-Webis (https://www.semanticscholar.org/paper/A-Stylometric-Inquiry-into-Hyperpartisan-and-Fake-Potthast-Kiesel/ed31e1225f6a76b469dfe4d022b235dc70be4390#citing-papers) in Semantic Scholar.**

2. Is there a repository that links to any or all papers or systems that use the dataset? If so, please provide a link or other access point. 
**Yes. In addition to the above, forks of the original repository for MisInfoText can be found in GitHub (https://github.com/sfu-discourse-lab/MisInfoText/forks).**

3. What (other) tasks could the dataset be used for? 
**This dataset can be used for any task that involves studying fake news, including its characteristics, detection and mitigation.**

4. Is there anything about the composition of the dataset or the way it was collected and preprocessed/cleaned/labeled that might impact future uses? For example, is there anything that a dataset consumer might need to know to avoid uses that could result in unfair treatment of individuals or groups (e.g., stereotyping, quality of service issues) or other risks or harms (e.g., legal risks, financial harms)? If so, please provide a description. Is there anything a dataset consumer could do to mitigate these risks or harms? 
**The scope of the dataset is limited for the following reasons:**
- **This dataset consists of 1371 English language articles based mostly on general interest or political news in the United States from a relatively small sample of publishers with a relatively narrow scope of political slants during the mid-2010s that have gone viral on social media.**
- **The data was curated by one sets of mainstream American fact-checking journalists (BuzzFeed).**
- **The dataset does not include common sources of false claims such as fraudulent/hoax sites.**
- **The article veracity label "no factual content" is very broad and does not take into account the nuances of satire, analysis articles, opinion articles, etc.**
- **There is evidence of gender bias, as only 22% of gendered mentions in the article text pertain to women, across all levels of article veracity (as measured by GenBit).**

5. Are there tasks for which the dataset should not be used? If so, please provide a description. 
**Per above, models trained on this dataset may not perform well on data outside this scope. A significant increase in size and expansion in scope of the dataset is necessary to both better understand fake news and build systems that can generalize on tasks such as fake news detection.**

**Different article sources may have different linguistic patterns/journalistic styles/idiosyncrasies. As such, a fake news detection model may only be recognizing linguistic patterns rather than fake news.**

**There are many ethical challenges that need to be evaluated before developing and applying a fake news detection model on systems that affect people, including, but not limited to:**
- **Could model features based on social media user handles or specific news sources be used to silence people?**
- **How much can historical data be used to detect fake news, given the constantly-evolving nature of fake news?**
- **To what extent can systems used for fake news detection in turn make it easier to develop systems for automatically generating fake news?**

6. Any other comments? 
**None.**

## Distribution
1. Will the dataset be distributed to third parties outside of the entity (e.g., company, institution, organization) on behalf of which the dataset was created? If so, please provide a description. 
**Yes. Both datasets have been distributed on the Internet.**

2. How will the dataset will be distributed (e.g., tarball on website, API, GitHub)? Does the dataset have a digital object identifier (DOI)? 
**The datasets have been distributed on GitHub and Zenodo: https://github.com/sfu-discourse-lab/Misinformation_detection (MisInfoText); https://zenodo.org/record/1239675 (BuzzFeed-Webis). There is not an associated DOI for MisInfoText. The DOI for BuzzFeed-Webis is 10.5281/zenodo.1239675.**

3. When will the dataset be distributed? 
**BuzzFeed-Webis was distributed in 2018. MisInfoText was distributed in 2019.**

4. Will the dataset be distributed under a copyright or other intellectual property (IP) license, and/or under applicable terms of use (ToU)? If so, please describe this license and/or ToU, and provide a link or other access point to, or otherwise reproduce, any relevant licensing terms or ToU, as well as any fees associated with these restrictions.
**The article content is under copyright unless stated otherwise. MisInfoText has a GPLv3 license, and BuzzFeed-Webis has a Creative Commons Attribution 4.0 International license. There are requests to cite the following papers if the datasets are used:**

```
Fatemeh Torabi Asr and Maite Taboada (2019) [MisInfoText](https://github.com/sfu-discourse-lab/MisInfoText). A collection of news articles, with false and true labels. Dataset.
```

```
Potthast, Martin, Kiesel, Johannes, Reinartz, Kevin, Bevendorff, Janek, & Stein, Benno. (2018). BuzzFeed-Webis Fake News Corpus 2016 (ACL 2018) [Data set]. The 56th Annual Meeting of the Association for Computational Linguistics (ACL 2018), Melbourne, Australia. Zenodo. https://doi.org/10.5281/zenodo.1239675
```



5. Have any third parties imposed IP-based or other restrictions on the data associated with the instances? If so, please describe these restrictions, and provide a link or other access point to, or otherwise reproduce, any relevant licensing terms, as well as any fees associated with these restrictions. 
**Other than the above, none.**

6. Do any export controls or other regulatory restrictions apply to the dataset or to individual instances? If so, please describe these restrictions, and provide a link or other access point to, or otherwise reproduce, any supporting documentation. 
**Unknown to the authors of the datasheet.**

7. Any other comments? 
**None.**

## Maintenance
1. Who will be supporting/hosting/maintaining the dataset?
**Maite Taboada is hosting the MisInfoText dataset.**

2. How can the owner/curator/manager of the dataset be contacted (e.g., email address)?
**The host of the MisInfoText dataset, Maite Taboada, can be contacted at https://www.sfu.ca/~mtaboada/.**

3. Is there an erratum? If so, please provide a link or other access point.
**There are no erratum given.**

4. Will the dataset be updated (e.g., to correct labeling errors, add new instances, delete instances)? If so, please describe how often, by whom, and how updates will be communicated to dataset consumers (e.g., mailing list, GitHub)? 
**Updates are posted to GitHub.**

5. If the dataset relates to people, are there applicable limits on the retention of the data associated with the instances (e.g., were the individuals in question told that their data would be retained for a fixed period of time and then deleted)? If so, please describe these limits and explain how they will be enforced. 
**N/A.**

6. Will older versions of the dataset continue to be supported/hosted/maintained? If so, please describe how. If not, please describe how its obsolescence will be communicated to dataset consumers. 
**Yes, changes are hosted on GitHub or Zenodo.**

7. If others want to extend/augment/build on/contribute to the dataset, is there a mechanism for them to do so? If so, please provide a description. Will these contributions be validated/verified? If so, please describe how. If not, why not? Is there a process for communicating/distributing these contributions to dataset consumers? If so, please provide a description. **Unknown to the authors of the datasheet.**

8. Any other comments? 
**None.**
